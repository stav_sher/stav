from bottle import route, run, template, static_file, request

@route('/')
def index():
    return template('index')

@route('/hello')
def hello():
    return "Hello World!"

@route('/static/<filename>')
def server_static(filename):
	return static_file(filename, root='/home/yoavi/stav/mklr')

@route('/dibrot')
def dibrot():
    return template('dibrot')

@route('/tofes')
def tofes():
	return template('tofes')

@route('/tofes2')
def show_future():
        friend = request.query.mybf
        grandma = request.query.mygrandma
        bus = request.query.mybus
        animal = request.query.myanimal
        curse = request.query.mycurse
        return template('future', friend=friend, grandma=grandma, bus=bus, animal=animal, curse=curse)

@route('/dibrot', method='POST')
def show_diber():
        mydiber = request.forms.mydiber
        mylist = [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
        ]
        diber = mylist[int(mydiber)-1]
        return template ('dibrot2', diber=diber)

run(host='localhost', port=8080, debug=True)